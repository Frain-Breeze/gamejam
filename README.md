# gamejam Framework © 2019 HaydenKow
## Free and Open for the Community!

## Working:
- 2D Rendering
- 3D Rendering
- Cross Platform: Dreamcast, PSP, Windows, Linux
- Sound
- Controller Input
- Scene Switching
- Smart Asset Handling
- Abstracted File IO

# Getting Started
## Docker
This library/framework was generally conceived to rely on docker containers to handle all cross platform build needs. As such the default ``Makefile`` will call on a seperate container to handle compilation of each platform.

If you have docker and would like to immediately start with no configuration, then feel free to use the default makefile system.
```bash
make
```
Instead, if you have local toolchains feel free to keep reading for an explanation on how to get started for each specific platform.

# Quick Start
## Dreamcast
 Needs Kallistios but most dependencies should be included when cloning
  
  ### Building the Library itself
   ```bash
   make -f makefile.dc 
   ```
  ### Building Examples
  ```bash
  make -f makefile.dc examples
  ```
  These will be built in the release folder for each example
  ### Your Own Project
  Feel free to copy an example project and build from there.
  Alternatively, if you copy makefile.dc from the ``makefiles`` folder you should be able to configure your project to another location.

  When ready to build:
  ```bash
  make -f makefile.dc cdi
  ``` 
----
## Windows (mingw)
 Needs mingw-w64-x64_86 but most dependencies should be included when cloning
  
  ### Building the Library itself
   ```bash
   make -f makefile.win 
   ```

  ### Building Examples
  ```bash
  make -f makefile.win examples
  ```
  These will be built in the release folder for each example

  ### Your Own Project
  Feel free to copy an example project and build from there.
  Alternatively, if you copy makefile.win from the ``makefiles`` folder you should be able to configure your project to another location.

  When ready to build:
  ```bash
  make -f makefile.win all
  ``` 
----
## Linux 
Needs gcc, glfw3, openal other dependencies should be included when cloning
  
  ### Building the Library itself
   ```bash
   make -f makefile.lin 
   ```

  ### Building Examples
  ```bash
  make -f makefile.lin examples
  ```
  These will be built in the release folder for each example

  ### Your Own Project
  Feel free to copy an example project and build from there.
  Alternatively, if you copy makefile.lin from the ``makefiles`` folder you should be able to configure your project to another location.

  When ready to build:
  ```bash
  make -f makefile.lin all
  ``` 
----
## PSP 
 Needs psp-toolchain other dependencies should be included when cloning
  
  ### Building the Library itself
   ```bash
   make -f makefile.psp
   ```
  ### Building Examples
  ```bash
  make -f makefile.psp examples
  ```
  These will be built in the release folder for each example
  ### Your Own Project
  Feel free to copy an example project and build from there.
  Alternatively, if you copy makefile.psp from the ``makefiles`` folder you should be able to configure your project to another location.

  When ready to build:
  ```bash
  make -f makefile.psp all
  ``` 
