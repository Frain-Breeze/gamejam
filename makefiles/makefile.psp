TARGET    ?= $(EXEC_DIR)/$(TARGET_EXEC)
PLATFORM = psp

BUILD_DIR ?= ./build/$(PLATFORM)
EXEC_DIR ?= ./release/$(PLATFORM)
SRC_DIRS ?= ./
ROOT_DIR ?= ../..
INCLUDE_DIR ?= $(ROOT_DIR)/include
DEP_DIR ?= $(ROOT_DIR)/deps
LIB_DIR ?= $(ROOT_DIR)/lib

SRCS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -name "*.cpp" -or -name "*.c" -or -name "*.s"))
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DEPS = $(SRC_DIRS) $(DEP_DIR)/stb/include $(DEP_DIR)/cglm/include  $(DEP_DIR)/libpspmath/include $(DEP_DIR)/pthreads-emb-1.0/include
INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -type d)) $(shell find $(INCLUDE_DIR) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS)) $(addprefix -I,$(INC_DEPS))

BASE_CFLAGS =  -g0 -DPSP -Wall -Wextra -Wshadow -Wstack-protector -Wstrict-prototypes -Wformat=0  -std=gnu11 -fsingle-precision-constant -fdiagnostics-color
RELEASE_CFLAGS = $(BASE_CFLAGS) -DNDEBUG -O3 -ffast-math -funsafe-math-optimizations -fomit-frame-pointer
DEBUG_CFLAGS = $(BASE_CFLAGS) -DDEBUG -O0 -ggdb -fno-inline -fno-omit-frame-pointer

# Use this when we release as a full iso to simplify
#ISO_FLAGS = -DPSP_ISO

CFLAGS    = $(DEBUG_CFLAGS)
CXXFLAGS  = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS   = $(CFLAGS)

LIB_PSPMATH = $(DEP_DIR)/libpspmath/libpspmath.a
LIB_PSPGL = $(DEP_DIR)/pspgl/libGL.a
LIB_PTHREADS = $(DEP_DIR)/pthreads-emb-1.0/libpthread-psp.a

LIBDIR    =
LDFLAGS   = -L$(DEP_DIR)/openal-psp/lib

LIBS    = $(LIB_DIR)/$(PLATFORM)/libgamejam.a $(LIB_PSPMATH) -lopenal-soft $(LIB_PTHREADS) -lglut -lGLU -lGL -lpspaudiolib -lpspaudio -lm -lc -lpspvfpu -lpspgum -lpspgu -lpsprtc 
INCS	  = $(INC_FLAGS) -isystem $(DEP_DIR) $(DEP_INCS)

BUILD_PRX = 1
PSP_FW_VERSION = 371

PSP_EBOOT = $(EXEC_DIR)/EBOOT.PBP
PSP_EBOOT_TITLE ?= $(TARGET) $(BUILDDATE)
#PSP_EBOOT_TITLE = Jump IT!
PSP_EBOOT_SFO ?= $(BUILD_DIR)/PARAM.SFO
#PSP_EBOOT_ICON ?= $(BUILD_DIR)/ICON0.PNG
PSP_EBOOT_ICON ?= NULL
PSP_EBOOT_ICON1 ?= NULL
#PSP_EBOOT_UNKPNG ?= $(BUILD_DIR)/PIC1.PNG
PSP_EBOOT_UNKPNG ?= NULL
PSP_EBOOT_PIC1 ?= NULL
PSP_EBOOT_SND0 ?= NULL
PSP_EBOOT_PSAR = NULL > /dev/null 2>&1

EXTRA_TARGETS   = $(PSP_EBOOT)

PSPSDK   = $(shell psp-config --pspsdk-path)
include $(ROOT_DIR)/makefiles/build.mak

all: $(PSP_EBOOT) msg $(LIB_PSPMATH) $(LIB_PSPGL)
default: $(PSP_EBOOT) msg $(LIB_PSPMATH) $(LIB_PSPGL)
FINAL_TARGET = 

msg: $(LIB_DIR)/$(PLATFORM)/libgamejam.a
	@echo -e "\n+  $(PSP_EBOOT)"
	@$(MKDIR_P) $(EXEC_DIR)
  ifneq ("$(wildcard $(SRC_DIRS)/assets/)","")
		@$(CP_R) $(SRC_DIRS)/assets/*  $(EXEC_DIR)/ 2>/dev/null || :
  endif
  ifneq ("$(wildcard $(SRC_DIRS)/assets_extra/eboot/)","")
		@$(CP_R) $(ROOT_DIR)/assets_extra/$(PLATFORM)/* $(EXEC_DIR)/ 2>/dev/null || :
  endif

$(BUILD_DIR)/%.c.o: %.c
	@echo "> $@"
	@$(MKDIR_P) $(dir $@)
	@$(MKDIR_P) $(EXEC_DIR)
	@$(CC) $(CPPFLAGS) $(CFLAGS) $(INCS) -c $< -o $@

$(BUILD_DIR)/%.prx: $(BUILD_DIR)/%.elf
	@$(MKDIR_P) $(EXEC_DIR)
	@$(CP_N) assets/* $(EXEC_DIR)/
	@$(CP_N) assets_eboot/* $(BUILD_DIR)/
	@psp-prxgen $< $@

.PHONY: pbp

pbp: $(PSP_EBOOT) msg $(LIB_PSPMATH) $(LIB_PSPGL)

MKDIR_P ?= mkdir -p
CP_N ?= cp -n
CP_F ?= cp -f
CP_R ?= cp -r -u
