LIN	:= $(shell command -v gcc 2> /dev/null)
WIN := $(shell command -v x86_64-w64-mingw32-gcc 2> /dev/null)
DC 	:= $(shell command -v sh-elf-gcc 2> /dev/null)
PSP	:= $(shell command -v psp-gcc 2> /dev/null)

ifndef MK_EXT
	ifdef WIN
		MK_EXT = win
	endif
endif
ifndef MK_EXT
	ifdef DC
			MK_EXT = dc
	endif
endif
ifndef MK_EXT
	ifdef PSP
			MK_EXT = psp
	endif
endif
ifndef MK_EXT
	ifdef LIN
			MK_EXT = lin
	endif
endif

ccred:=$(shell echo "\033[0;31m")
ccyellow:=$(shell echo "\033[0;33m")
ccend:=$(shell echo "\033[0m")

ifeq ($(MK_EXT),)
$(warning $(ccred) $$(MK_EXT) is undefined, Please choose between: dc, psp, lin, win $(ccend))
$(error   $(ccyellow)  Ex: make -C $(TARGET_EXEC) MK_EXT=dc $(ccend) )
endif

NAME ?= $(TARGET_EXEC)

ifeq ($(MK_EXT), psp)
PSP_EBOOT_TITLE = $(NAME)
endif

include ../../makefiles/makefile.$(MK_EXT)