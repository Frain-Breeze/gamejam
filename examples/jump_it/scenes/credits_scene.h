/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\credits_scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 24th 2019, 8:08:25 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef CREDITS_SCENE_H
#define CREDITS_SCENE_H

#include <common.h>

extern scene scene_credits;

#endif /* CREDITS_SCENE_H */
