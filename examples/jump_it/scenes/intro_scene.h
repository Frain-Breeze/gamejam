/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\intro\intro_scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\intro
 * Created Date: Tuesday, November 19th 2019, 12:54:12 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef INTRO_SCENE_H
#define INTRO_SCENE_H

#include <common.h>

float time_waiting;
char current_intro;

extern scene intro_scene;

#endif /* INTRO_SCENE_H */
