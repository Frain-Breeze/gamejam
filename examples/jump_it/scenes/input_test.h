/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\input_test.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Saturday, November 16th 2019, 1:22:43 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef INPUT_TEST_H
#define INPUT_TEST_H

#include <common.h>

extern scene scene_input_test;

#endif /* INPUT_TEST_H */
