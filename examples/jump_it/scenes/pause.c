/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "pause.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

#include "world_2d/world.h"

static void Pause_Init(void);
static void Pause_Update(float time);
static void Pause_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(scene_pause, &Pause_Init, NULL, &Pause_Render2D, NULL, &Pause_Update, SCENE_FALLTHROUGH_RENDER | SCENE_NO_REINIT_CHILD);

static char pause_active;

static void Pause_Init(void)
{
    pause_active = 0;
}

static void Pause_Update(float time)
{
    (void)time;
    if (!pause_active)
    {
        if (INPT_ButtonEx(BTN_START, BTN_RELEASE))
        {
            pause_active = 1;
        }
    }
    else
    {
        if (INPT_ButtonEx(BTN_START, BTN_RELEASE))
        {
            scene_current = pop(&scene_root);
        }
        if (INPT_Button(BTN_Y))
        {
            pop(&scene_root);
            push(&scene_root, scene_game);
            push(&scene_root, level_select);
            WRLD_2D_Destroy();
            scene_current = pop(&scene_root);
        }
    }
}

static void Pause_Render2D(float time)
{
    (void)time;
    UI_DrawFadeScreen();

    /* Draw Box */
    int height = 200;
    int width = (int)(height * 1.5f);
    int thickness = 4;

    dimen_RECT quad = {.x = 640 / 2 - width / 2, .y = 480 / 2 - height / 2, .w = width, .h = height};
    UI_DrawFill(&quad, 0.0f, 0.0f, 0.0f);

    quad.w = width;
    quad.h = thickness;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f); //Top
    quad.w = thickness;
    quad.h = height;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f); //Left
    quad.y = (480 / 2 + height / 2) - thickness;
    quad.w = width;
    quad.h = thickness;

    UI_DrawFill(&quad, 0, 255.0f, 255.0f); //Bottom

    quad.x = 640 / 2 + width / 2;
    quad.y = 480 / 2 - height / 2;
    quad.w = thickness;
    quad.h = height;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f); //Right

    /* Now Text */
    UI_TextColor(1.0f, 0, 0);
    UI_TextSize(32);
    UI_DrawStringCentered(640 / 2, 480 / 2 - UI_TextSizeGet(), "PAUSED");
    UI_TextSize(20);
    UI_DrawStringCentered(320 + 32, 270, " Resume");
    UI_DrawStringCentered(320 + 32, 318, " EXIT");

    UI_TextSize(32);
    UI_TextColor(1, 1, 1);
    UI_DrawStringCentered(640 / 2 - (32 * 2), 270, STRING_START_BTN);
    UI_DrawStringCentered(640 / 2 - (32 * 2), 318, STRING_Y_BTN);
}
