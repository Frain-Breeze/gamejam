#ifndef PLAYER_H
#define PLAYER_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\player.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Monday, July 8th 2019, 11:00:36 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include <common.h>
#include "voxel_physics/voxel_physics.h"

extern rigidbody *player;

void PLYR_Draw(void);
void PLYR_Destroy(void);
void PLYR_Create(void);
void PLYR_Update(void);

#endif /* PLAYER_H */
