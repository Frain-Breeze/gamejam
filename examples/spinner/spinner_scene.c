/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "spinner_scene.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <common/obj_loader.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("spinner_example", WINDOWED);

static void Spinner_Init(void);
static void Spinner_Exit(void);
static void Spinner_Update(float time);
static void Spinner_Render2D(float time);
static void Spinner_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);
SCENE(scene_spinner, &Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);

static model_obj *spinner_obj;
static tx_image *spinner_tx;

model_obj *spinner_obj1;
model_obj *spinner_obj2;
model_obj *spinner_obj3;

static void Spinner_Init(void)
{
    spinner_obj = OBJ_load("basicCharacter.obj");
    spinner_obj1 = OBJ_load("basicCharacter.obj");
    spinner_obj2 = OBJ_load("basicCharacter.obj");
    spinner_obj3 = OBJ_load("basicCharacter.obj");
 
    spinner_tx = IMG_load("skin_man.png");
    RNDR_CreateTextureFromImage(spinner_tx);
    spinner_obj->texture = spinner_tx->id;
}

static void Spinner_Exit(void)
{
    resource_object_remove(spinner_tx->crc);
    resource_object_remove(spinner_obj->crc);
    resource_object_remove(spinner_obj1->crc);
    resource_object_remove(spinner_obj2->crc);
    resource_object_remove(spinner_obj3->crc);
    OBJ_destroy(spinner_obj);
}

static void Spinner_Update(float time)
{
    (void)time;
    if (INPT_ButtonEx(BTN_START, BTN_RELEASE))
    {
        Sys_Quit();
    }
}

static void Spinner_Render2D(float time)
{
    (void)time;
    dimen_RECT pos = {0, 0, 256, 256};
    UI_DrawPicDimensions(&pos, spinner_tx);

    UI_TextSize(32);
    UI_DrawStringCentered(320, 440, "Press START to exit!");
}

static void Spinner_Render3D(float time)
{
    glPushMatrix();

    static float counter = 0.0f;
    counter += (time * 60);

    gluLookAt(0, 10, 30, 0, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f); // angled side from front

    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    GL_Bind(spinner_tx);

    glVertexPointer(3, GL_FLOAT, 5 * sizeof(float), spinner_obj->tris);
    glTexCoordPointer(2, GL_FLOAT, 5 * sizeof(float), ((float *)spinner_obj->tris) + 3);

    glTranslatef(0, 0, 0.0f);
    glRotatef(counter, 0, 1, 0);

    //float normalizeAmount = MIN(1 / (spinner_obj.max[0] - spinner_obj.min[0]), 1 / (spinner_obj.max[2] - spinner_obj.min[2]));
    //glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
    glDrawArrays(GL_TRIANGLES, 0, spinner_obj->num_tris);
    glPopMatrix();
}
