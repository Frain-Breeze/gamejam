/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */

#include "mdl_view.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("mdl_viewer", WINDOWED);

static void MDLView_Init(void);
static void MDLView_Exit(void);
static void MDLView_Update(float time);
static void MDLView_Render2D(float time);
static void MDLView_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&MDLView_Init, &MDLView_Exit, &MDLView_Render2D,
              &MDLView_Render3D, &MDLView_Update, SCENE_BLOCK);
SCENE(scene_input_test, &MDLView_Init, NULL, &MDLView_Render2D,
      &MDLView_Render3D, &MDLView_Update, SCENE_BLOCK);

extern void loadMDL(const char *filename);
extern void cleanup(void);
extern void display_bytes(int mode, int amount);
extern void display_floats(int mode, int amount);
extern void display_native(int mode, int amount);
extern int number_verts(void);

static char *mode_text[4] = {"Immediate", "Arrays", "pvr_fast_vert", "map buffer"};
static char *type_text[3] = {"Bytes/Raw", "Floats", "native"};
static int current_mode;
static int current_type;
static int current_amount;
static float time_between_input;
int mdl_load_failed;

#define INPUT_TIMEOUT 0.25f

static void MDLView_Init(void) {
  mdl_load_failed = 0;
  loadMDL(transform_path("player.mdl"));
  current_mode = 0;
  current_type = 0;
  current_amount = 0;
  time_between_input = 0.0f;
}

static void MDLView_Exit(void) { cleanup(); }

static void MDLView_Update(float time) {
  time_between_input -= time;

  if (time_between_input < 0.0f) {
    if (INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
      if (++current_mode > 3) {
        current_mode = 0;
      }
      time_between_input = INPUT_TIMEOUT;
    }
    if (INPT_ButtonEx(BTN_B, BTN_RELEASE)) {
      if (++current_type > 2) {
        current_type = 0;
      }
      time_between_input = INPUT_TIMEOUT;
    }
    if (INPT_DPADDirection(DPAD_LEFT)) {
      if (--current_amount < 0) {
        current_amount = 0;
      }
      time_between_input = INPUT_TIMEOUT;
    }
    if (INPT_DPADDirection(DPAD_RIGHT)) {
      if (++current_amount >= 9) {
        current_amount = 8;
      }
      time_between_input = INPUT_TIMEOUT;
    }
  }
  if (INPT_Button(BTN_START)) {
    Sys_Quit();
  }
}

static void MDLView_Render3D(float time) {
  if (!mdl_load_failed) {
    (void)time;
    glPushMatrix();
    switch (current_type) {
      case 0:
        display_bytes(current_mode, current_amount);
        break;
      case 1:
        display_floats(current_mode, current_amount);
        break;
      case 2:
        display_native(current_mode, current_amount);
        break;
    }

    glPopMatrix();
  }
}

static void MDLView_Render2D(float time) {
  if (mdl_load_failed) {
    UI_TextSize(32);
    UI_TextColor(1, 0, 0);
    UI_DrawStringCentered(320, 240, "ERROR loading MDL");
    return;
  }

  UI_TextSize(14);
  UI_TextColorEx(1, 1, 1, 1);
  char mspf[16];
  {
    float current_mspf = ((1000.0f / time) / 1000.0f);
    snprintf(mspf, 16, "%2.02f fps", current_mspf > 60.0f ? 60.0f : current_mspf);
  }
  UI_TextColor(1, 1, 0);
  UI_DrawStringCentered(640 - (4.5 * 16), 420, mspf);

  snprintf(mspf, 16, "%05d verts", number_verts());
  UI_TextColor(1, 1, 1);
  UI_DrawStringCentered((6 * 16), 420, mspf);

  /* Faster than sprintf? */
  char message[32] = {};
  strcat(message, mode_text[current_mode]);
  strcat(message, " - ");
  strcat(message, type_text[current_type]);
  UI_DrawStringCentered(320, 440, message);

  /* Draw Format Change Message */
  UI_DrawStringCentered(320, 460, "A to change mode!B to change data format!");

  if ((current_type == 2) && (current_mode != 2)) {
    UI_TextSize(32);
    UI_TextColor(1, 0, 0);
    UI_DrawStringCentered(320, 240, "N/A");
  }
}
