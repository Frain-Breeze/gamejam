#pragma once
#ifndef GL_FAST_VERT_H
#define GL_FAST_VERT_H
/*
 * Filename:
 * d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\mdl_viewer\support\gl_fast_vert.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\mdl_viewer\support
 * Created Date: Tuesday, January 28th 2020, 12:00:56 am
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2020 HaydenKow
 */

#define PACK_BGRA8888(b,g,r,a) ((uint32_t)(((uint8_t)b << 24) + ((uint8_t)g << 16) + ((uint8_t)r << 8) + (uint8_t)a))
#define VTX_COLOR_WHITE .color = { .packed = PACK_BGRA8888(255,255,255,255)}

typedef struct __attribute__((packed, aligned(4))) uv_float {
  float x, y, z;
} vec3f;

typedef struct __attribute__((packed, aligned(4))) uv_float {
  float u, v;
} uv_float;

typedef struct __attribute__((packed)) color_uc_struct {
  unsigned char b, g, r, a;
} color_uc_struct;

typedef union color_uc {
  color_uc_struct array;
  unsigned int packed;
} color_uc;

typedef struct __attribute__((packed, aligned(4))) glvert_fast_t {
  uint32_t flags;
  struct vec3f_gl vert;
  uv_float texture;
  color_uc color;  // bgra
  union {
    float pad;
    unsigned int vertindex;
  } pad0;
} glvert_fast_t;

#endif /* GL_FAST_VERT_H */
