#ifndef MDL_H
#define MDL_H

#include "common.h"

#include <common/renderer.h>

/* MDL header */
struct mdl_header_t {
  int ident;   /* magic number: "IDPO" */
  int version; /* version: 6 */

  vec3 scale;     /* scale factor */
  vec3 translate; /* translation vector */
  float boundingradius;
  vec3 eyeposition; /* eyes' position */

  int num_skins;  /* number of textures */
  int skinwidth;  /* texture width */
  int skinheight; /* texture height */

  int num_verts;  /* number of vertices */
  int num_tris;   /* number of triangles */
  int num_frames; /* number of frames */

  int synctype; /* 0 = synchron, 1 = random */
  int flags;    /* state flag */
  float size;
};

/* Skin */
struct mdl_skin_t {
  int group;     /* 0 = single, 1 = group */
  GLubyte *data; /* texture data */
};

/* Texture coords */
struct mdl_texcoord_t {
  int onseam;
  int s;
  int t;
};

/* Triangle info */
struct mdl_triangle_t {
  int facesfront; /* 0 = backface, 1 = frontface */
  int vertex[3];  /* vertex indices */
};

/* Compressed vertex */
struct mdl_vertex_t {
  unsigned char v[3];
  unsigned char normalIndex;
};

/* Simple frame */
struct mdl_simpleframe_t {
  struct mdl_vertex_t bboxmin; /* bouding box min */
  struct mdl_vertex_t bboxmax; /* bouding box max */
  char name[16];
  struct mdl_vertex_t *verts; /* vertex list of the frame */
};

/* Simple frame */
struct mdl_simpleframe_float_t {
  struct mdl_vertex_t bboxmin; /* bouding box min */
  struct mdl_vertex_t bboxmax; /* bouding box max */
  char name[16];
  vec3 *verts; /* vertex list of the frame in floats */
};

/* Simple frame */
struct mdl_simpleframe_native_t {
  struct mdl_vertex_t bboxmin; /* bouding box min */
  struct mdl_vertex_t bboxmax; /* bouding box max */
  char name[16];
  glvert_fast_t *verts; /* vertex list of the frame in native pvr format */
};

/* Model frame */
struct mdl_frame_t {
  int type;                       /* 0 = simple, !0 = group */
  struct mdl_simpleframe_t frame; /* this program can't read models
                                     composed of group frames! */
};

/* Model frame */
struct mdl_frame_float_t {
  int type;                             /* 0 = simple, !0 = group */
  struct mdl_simpleframe_float_t frame; /* this program can't read models
                                     composed of group frames! */
};

/* Model frame */
struct mdl_frame_native_t {
  int type;                              /* 0 = simple, !0 = group */
  struct mdl_simpleframe_native_t frame; /* this program can't read models
                                     composed of group frames! */
};

/* MDL model structure */
struct mdl_model_t {
  struct mdl_header_t header;

  struct mdl_skin_t *skins;
  struct mdl_texcoord_t *texcoords;
  struct mdl_triangle_t *triangles;
  struct mdl_frame_t *frames;
  struct mdl_frame_float_t *frames_floats;
  struct mdl_frame_native_t *frames_native;

  vec2 *texcoords_floats;

  GLuint *tex_id;
  int iskin;
};

#endif /* MDL_H */
