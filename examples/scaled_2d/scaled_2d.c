/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d\scaled_2d.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d
 * Created Date: Monday, April 13th 2020, 5:03:20 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#include "scaled_2d.h"

#include <common/Input.h>
#include <common/image_loader.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("Scaled 2D | Integrity", WINDOWED);

static void Scaled_Init(void);
static void Scaled_Exit(void);
static void Scaled_Update(float time);
static void Scaled_Render2D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Scaled_Init, &Scaled_Exit, &Scaled_Render2D, NULL, &Scaled_Update, SCENE_BLOCK);
SCENE(scene_scaled_test, &Scaled_Init, NULL, &Scaled_Render2D, NULL, &Scaled_Update, SCENE_BLOCK);

static int Scaled_loaded = 0;

static tx_image *Scaled_sheet;
static sprite btn_a[2];
static sprite btn_b[2];
static sprite btn_x[2];
static sprite btn_y[2];
static sprite btn_start[2];

static sprite dpad_up[2];
static sprite dpad_down[2];
static sprite dpad_left[2];
static sprite dpad_right[2];

static tx_image *ui_texture;
static sprite circle, circle_trans;

static void Scaled_Init(void) {
  if (!Scaled_loaded) {
    Scaled_sheet = IMG_load("Scaled_sheet_half.png");
    RNDR_CreateTextureFromImage(Scaled_sheet);

    /* Buttons */
    // Dark, unlit
    btn_a[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 220, 2, 80, 80, 0.5f);
    btn_b[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 302, 2, 80, 80, 0.5f);
    btn_x[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 384, 2, 80, 80, 0.5f);
    btn_y[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 2, 84, 80, 80, 0.5f);
    btn_start[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 248, 84, 108, 48, 0.5f);
    // Light, activated
    btn_a[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 220, 166, 80, 80, 0.5f);
    btn_b[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 302, 166, 80, 80, 0.5f);
    btn_x[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 384, 166, 80, 80, 0.5f);
    btn_y[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 2, 248, 80, 80, 0.5f);
    btn_start[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 248, 248, 108, 48, 0.5f);

    /* Dpad/Analog emu */
    // Dark, unlit
    dpad_up[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 358, 84, 61, 76, 0.5f);
    dpad_down[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 157, 2, 61, 75, 0.5f);
    dpad_left[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 2, 2, 76, 61, 0.5f);
    dpad_right[0] = IMG_create_sprite_scaled_alt(Scaled_sheet, 80, 2, 75, 61, 0.5f);
    // Light, activated
    dpad_up[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 421, 84, 61, 76, 0.5f);
    dpad_down[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 157, 166, 61, 75, 0.5f);
    dpad_left[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 2, 166, 76, 61, 0.5f);
    dpad_right[1] = IMG_create_sprite_scaled_alt(Scaled_sheet, 80, 166, 75, 61, 0.5f);

    /* Load Assets */
    ui_texture = IMG_load("ui_stuff.png");
    RNDR_CreateTextureFromImage(ui_texture);
    circle = IMG_create_sprite_alt(ui_texture, 2, 2, 32, 32);
    circle_trans = IMG_create_sprite_alt(ui_texture, 36, 2, 32, 32);
    UI_SetVirtualToReal();
  }
  Scaled_loaded = 1;
}

static void Scaled_Exit(void) {
  resource_object_remove(Scaled_sheet->crc);
  resource_object_remove(ui_texture->crc);
}

static void Scaled_Update(float time) {
  (void)time;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
  if (INPT_TriggerPressed(TRIGGER_L)) {
    Sys_SetFullscreen(false);
  }

  if (INPT_TriggerPressed(TRIGGER_R)) {
    Sys_SetFullscreen(true);
  }
}

static void circleSym8(int xCenter, int yCenter, int radius) {
  dimen_RECT pos = {xCenter - radius, yCenter - radius, radius * 2, radius * 2};
  UI_DrawTransSprite(&pos, 1.0f, &circle_trans);
}

static void Scaled_Render2D(float time) {
  (void)time;
#ifdef PSP
#define V_WIDTH 480
#define V_HEIGHT 272
#else
#define V_WIDTH 320
#define V_HEIGHT 240
#endif

  /* SCR_WIDTH & SCR_HEIGHT are set to physical screen dimensions for each platform */

  /* Green Elements are new virtual resolution */
  dimen_RECT item2 = {V_WIDTH / 2, V_HEIGHT / 2, 100, 100};
  item2.x -= item2.w / 2;
  item2.y -= item2.h / 2;
  UI_SetScalingEnabled(true);
  UI_TextColor(0, 1, 0);
  DrawQuad_NoTex(&item2);
  UI_DrawStringCentered(V_WIDTH / 2, 30, "Virtual");
  UI_DrawCharacterCentered(V_WIDTH / 2, 10, 'V');
  circleSym8(V_WIDTH / 2, V_HEIGHT / 2, (V_HEIGHT / 2));

  /* Red Elements are native 640x480 resolution */
  dimen_RECT item = {640 / 2, 480 / 2, 100, 100};
  item.x -= item.w / 2;
  item.y -= item.h / 2;
  UI_SetScalingEnabled(false);
  UI_TextColor(1, 0, 0);
  DrawQuad_NoTex(&item);
  UI_DrawStringCentered(640 / 2, 420, "Reallll");
  UI_DrawCharacterCentered(620 / 2, 440, 'R');
  circleSym8(640 / 2, 480 / 2, 240);
}
