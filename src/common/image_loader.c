/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\image_loader.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 9:14:19 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "image_loader.h"

#include "renderer.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_FAILURE_STRINGS

#define STBI_NO_BMP
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM

#include <stb_image.h>

static tx_image *IMG_load_internal(const char *path) {
  tx_image *img = NULL;
  uint32_t crc = 0;
  int length = strlen(path);
  //crc32(&crc, (uint8_t *)&path, length<8?length:8);
  //crc32(&crc, (uint8_t *)path, 8);
  crc32(&crc, (uint8_t *)path, length);

  if (resource_object_find(crc) == -1) {
    img = (tx_image *)malloc(sizeof(tx_image));
    memset(img, 0, sizeof(tx_image));

    /* Check if file even exists */
    if (!Sys_FileExists(path)) {
      return img;
    }

    strncpy(img->name, path, 16);
    img->name[15] = '\0';
    img->crc = crc;
    int channels;
    stbi_set_flip_vertically_on_load(false);  // maybe true?
    img->data = stbi_load(path,
                          &img->width,
                          &img->height,
                          &channels,
                          STBI_rgb_alpha);  //maybe should be rgb
    //const char *stbi_reason = stbi_failure_reason();
    if (img->data == NULL) {
#ifdef DEBUG
      printf("ERROR: for path(%s)\n", path);
#endif
      return img;
    }
#ifdef DEBUG
    printf("Adding new Image! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('i', crc, img);
    return img;
  } else {
#ifdef DEBUG
    printf("Found already loaded Image! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('i', crc, img);
    return (tx_image *)(resource_object_pointer(crc));
  }
}

tx_image *IMG_load_from_memory(const unsigned char *buffer, int len) {
  tx_image *img = NULL;

  uint32_t crc = 0;
  crc32(&crc, (uint8_t *)&buffer, 4);

  if (resource_object_find(crc) == -1) {
    img = (tx_image *)malloc(sizeof(tx_image));
    memset(img, 0, sizeof(tx_image));
    snprintf(img->name, 16, "0x%p" PRIXPTR, (void *)buffer);
#ifdef DEBUG
    printf("%s\n", img->name);
#endif
    img->name[15] = '\0';
    img->crc = crc;
    int channels;
    stbi_set_flip_vertically_on_load(false);  // maybe true?
    img->data = stbi_load_from_memory((const unsigned char *)buffer,
                                      len,
                                      &img->width,
                                      &img->height,
                                      &channels,
                                      STBI_rgb_alpha);  //maybe should be rgb
    //const char *stbi_reason = stbi_failure_reason();
    if (img->data == NULL) {
#ifdef DEBUG
      printf("ERROR: for memory loaded image (0x%" PRIXPTR ")\n", (uintptr_t)buffer);
#endif
      return img;
    }
#ifdef DEBUG
    printf("Adding new Image! crc: %08X, 0x%" PRIXPTR "\n", (unsigned int)crc, (uintptr_t)buffer);
#endif
    resource_object_add('i', crc, img);
    return img;
  } else {
#ifdef DEBUG
    printf("Found already loaded Image! crc:  %08X, 0x%" PRIXPTR "\n", (unsigned int)crc, (uintptr_t)buffer);
#endif
    resource_object_add('i', crc, img);
    return (tx_image *)(resource_object_pointer(crc));
  }
}

tx_image *IMG_load(const char *path) {
  return IMG_load_internal(transform_path(path));
}

tx_image *IMG_load_boolean(const char *path, bool transform) {
  return IMG_load_internal((transform) ? transform_path(path) : path);
}

void IMG_unload(tx_image *img) {
  if (img->data != NULL) {
    stbi_image_free(img->data);
    //printf("\tFreed Image [%s]!\n", img->name);
    img->data = NULL;
  }
}

void IMG_destroy(tx_image *img) {
  IMG_unload(img);
  if (glIsTexture(img->id))
    glDeleteTextures(1, &img->id);

  //printf("\tDeleted Image [%s]!\n", img->name);
  memset(img, 0, sizeof(tx_image));
}

sprite IMG_create_sprite(tx_image *img, int x, int y, int x2, int y2) {
  sprite spr = (sprite){.parent = img,
                        .u = (float)(x / (float)img->width),
                        .v = (float)(y / (float)img->height),
                        .width = (float)((x2 - x) / (float)img->width),
                        .height = (float)((y2 - y) / (float)img->height),
                        .i_width = x2 - x,
                        .i_height = y2 - y};

  return spr;
}

sprite IMG_create_sprite_scaled(tx_image *img, int x, int y, int x2, int y2, float scale) {
  return IMG_create_sprite(img, (int)(x * scale), (int)(y * scale), (int)(x2 * scale), (int)(y2 * scale));
}