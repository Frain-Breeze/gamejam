/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\renderer.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Friday, July 5th 2019, 8:07:50 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "renderer.h"

#include "common.h"
#include "image_loader.h"

#define WIDE_ASPECT (16.0f / 9.0f)
#define SQUARE_ASPECT (4.0f / 3.0f)

static bool video_aspect_wide = false;

/* A general OpenGL initialization function.  Sets all of the initial parameters. */
void RNDR_Init(int Width, int Height)  // We call this right after our OpenGL window is created.
{
  glClearDepth(1.0);        // Enables Clearing Of The Depth Buffer
  glDepthFunc(GL_LESS);     // The Type Of Depth Test To Do
  glEnable(GL_DEPTH_TEST);  // Enables Depth Testing
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);
  glDisable(GL_BLEND);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();  // Reset The Projection Matrix

  gluPerspective(45.0f, (video_aspect_wide) ? WIDE_ASPECT : SQUARE_ASPECT, 0.1f, 100.0f);  // Calculate The Aspect Ratio Of The Window

  glMatrixMode(GL_MODELVIEW);
  glEnableClientState(GL_VERTEX_ARRAY);

  SCR_WIDTH = Width;
  SCR_HEIGHT = Height;
}

void RNDR_Reset(void)  // We call this right after our OpenGL window is created.
{
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);  // This Will Clear The Background Color To Black
  glClearDepth(1.0);                     // Enables Clearing Of The Depth Buffer
  glDepthFunc(GL_LESS);                  // The Type Of Depth Test To Do
  glEnable(GL_DEPTH_TEST);               // Enables Depth Testing
  glShadeModel(GL_SMOOTH);               // Enables Smooth Color Shading
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);
  glDisable(GL_BLEND);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();  // Reset The Projection Matrix

  gluPerspective(45.0f, (video_aspect_wide) ? WIDE_ASPECT : SQUARE_ASPECT, 0.1f, 100.0f);  // Calculate The Aspect Ratio Of The Window

  glMatrixMode(GL_MODELVIEW);
  glEnableClientState(GL_VERTEX_ARRAY);
}

/* The function called when our window is resized (which shouldn't happen, because we're fullscreen) */
void RNDR_Resize(int Width, int Height) {
  if (Height == 0)  // Prevent A Divide By Zero If The Window Is Too Small
    Height = 1;

  glViewport(0, 0, Width, Height);  // Reset The Current Viewport And Perspective Transformation

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
  glMatrixMode(GL_MODELVIEW);
}

void RNDR_SetWidescreen(bool wide) {
  video_aspect_wide = wide;
}

void RNDR_FlipAspect(void) {
  RNDR_SetWidescreen(!video_aspect_wide);
}

// Routine to load an tx_image as a texture.
GLuint RNDR_CreateTextureFromImage(tx_image *img) {
  if ((img->id == 0) && (img->data == NULL)) {
    return 0;
  } else if ((img->id != 0) && (img->data == NULL)) {
    return img->id;
  }
  glGenTextures(1, &(img->id));
  if (img->id) {
    glBindTexture(GL_TEXTURE_2D, img->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->width, img->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
    IMG_unload(img);
    return img->id;
  } else {
    return 0;
  }
}