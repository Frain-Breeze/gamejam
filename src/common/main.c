#include "common.h"
#include "private.h"
#include "renderer.h"
#include "input.h"
#include "image_loader.h"
#include "ui/ui_backend.h"

char error_str[64];
extern struct scene null_scene;

void Game_Main(__attribute__((unused)) int argc, __attribute__((unused)) char **argv)
{
    /* Start up Resource Manager */
    resource_test();

    RNDR_Init(SCR_WIDTH, SCR_HEIGHT);

    /* Can do setup here if you NEED/WANT it to run before anything else */
    UI_Init();

    //Setup our basic scene as bottom
    extern struct StackNode *scene_root;
    scene_root = malloc(sizeof(struct StackNode));
    scene_root->next = NULL;
    memcpy(&scene_root->data, &null_scene, sizeof(scene));
    memcpy(&scene_current, &null_scene, sizeof(scene));
}

void Game_Exit(void)
{
    SCN_Pop();
    UI_Destroy();
}

void Host_Input(__attribute__((unused)) float time)
{
}

static inline void Host_Render3D(float time)
{
    RNDR_Reset();
    if (scene_current.flags & SCENE_FALLTHROUGH_RENDER)
    {
        scene temp = peek(scene_root);
        if (temp.render3D)
            (*temp.render3D)(time);
    }
    if (scene_current.render3D)
        (*scene_current.render3D)(time);
}

static inline void Host_Render2D(float time)
{
    UI_Set2D();
    UI_TextColorEx(1.0f, 1.0f, 1.0f, 1.0f);
    if (scene_current.flags & SCENE_FALLTHROUGH_RENDER)
    {
        scene temp = peek(scene_root);
        if (temp.render2D)
            (*temp.render2D)(time);
    }
    if (scene_current.render2D)
        (*scene_current.render2D)(time);
}

void Host_Update(float time)
{
    if (scene_current.update)
        (*scene_current.update)(time);

    if (scene_current.flags & SCENE_FALLTHROUGH_UPDATE)
    {
        scene temp = peek(scene_root);
        if (temp.update)
            (*temp.update)(time);
    }
}

void Host_Frame(float time)
{
    /* Render Both parts */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
    glLoadIdentity();                                   // Reset The View
    Host_Render3D(time);
    Host_Render2D(time);
}
