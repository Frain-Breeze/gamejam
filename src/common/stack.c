#include "stack.h"

static int skip_init = 0;

struct StackNode *newNode(scene data)
{
    struct StackNode *stackNode = (struct StackNode *)malloc(sizeof(struct StackNode));
    stackNode->data = data;
    stackNode->next = NULL;
    return stackNode;
}

bool isEmpty(struct StackNode *root)
{
    return !root;
}

void push(struct StackNode **root, scene data)
{
    struct StackNode *stackNode = newNode(data);
    stackNode->next = *root;
    *root = stackNode;
}

scene pop(struct StackNode **root)
{
    if (isEmpty(*root))
        return (scene){.flags = INT_MIN};

    struct StackNode *temp = *root;
    *root = (*root)->next;
    struct scene popped = temp->data;

    /* Call Init */
    if (popped.init && !skip_init)
        (*popped.init)();

    /* Call Exit */
    if (scene_current.exit)
        (*scene_current.exit)();

    resource_objects_flush();
    //resource_print_objects();

    skip_init = 0;
    if (temp->data.flags & SCENE_NO_REINIT_CHILD)
        skip_init = 1;

    free(temp);

    return popped;
}

scene peek(struct StackNode *root)
{
    if (isEmpty(root))
        return (scene){.flags = INT_MIN};
    return root->data;
}
