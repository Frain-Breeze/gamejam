/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "common.h"
#include "ui/ui_backend.h"

static void Null_Update(float time);
static void Null_Render2D(float time);

struct scene null_scene = {
    .init = NULL,
    .render2D = &Null_Render2D,
    .render3D = NULL,
    .update = &Null_Update,
    .flags = SCENE_BLOCK,
};

extern scene scene_start;

static void Null_Update(float time)
{
    (void)time;
    SCN_ChangeTo(scene_start);
}

static void Null_Render2D(float time)
{
    (void)time;
    /*
    UI_TextSize(32);
    UI_TextColor(1,0,0);
    UI_DrawStringCentered(320, 240, "FATAL ERROR!");
    */
}
