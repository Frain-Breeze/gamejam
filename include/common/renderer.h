/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\common\renderer.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\common
 * Created Date: Friday, June 28th 2019, 8:34:48 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#pragma once 

#include "common.h"

#ifdef _arch_dreamcast
#include "gl.h"
#include "glext.h"
#include "glkos.h"
#include "glu.h"
#endif
#if defined(__MINGW32__) || defined(__linux__)
#define GLFW_INCLUDE_GLU
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#endif
#ifdef PSP
#include <GL/gl.h>   // Header File For The OpenGL32 Library
#include <GL/glu.h>  // Header File For The GLu32 Library
#include <GLES/egl.h>
#undef psp_log
extern void __pspgl_log(const char *fmt, ...);
/* disable verbose logging to "ms0:/log.txt" */
#if 1
#define psp_log(x...) __pspgl_log(x)
#else
#define psp_log(x...) \
  do {                \
  } while (0)
#endif

/* enable EGLerror logging to "ms0:/log.txt" */
#if 1
#define EGLCHK(x)                                \
  do {                                           \
    EGLint errcode;                              \
    x;                                           \
    errcode = eglGetError();                     \
    if (errcode != EGL_SUCCESS) {                \
      __pspgl_log("%s (%d): EGL error 0x%04x\n", \
                  __FUNCTION__, __LINE__,        \
                  (unsigned int)errcode);        \
    }                                            \
  } while (0)
#else
#define EGLCHK(x) x
#endif
#endif

#include "image_loader.h"

#define VERTEX_EOL 0xf0000000
#define VERTEX 0xe0000000

#define PACK_BGRA8888(b,g,r,a) ((uint32_t)(((uint8_t)(b) << 24) + ((uint8_t)(g) << 16) + ((uint8_t)(r) << 8) + (uint8_t)(a)))
#define VTX_COLOR_WHITE .color = { .packed = PACK_BGRA8888(255,255,255,255)}

typedef struct __attribute__((packed, aligned(4))) vec3f_gl {
  float x, y, z;
} vec3f;

typedef struct __attribute__((packed, aligned(4))) uv_float {
  float u, v;
} uv_float;

typedef union color_uc {
  unsigned char array[4];
  unsigned int packed;
} color_uc;

typedef struct __attribute__((packed, aligned(4))) glvert_fast_t {
  uint32_t flags;
  struct vec3f_gl vert;
  uv_float texture;
  color_uc color;  //bgra
  union {
    float pad;
    unsigned int vertindex;
  } pad0;
} glvert_fast_t;

#define glCheckError() glCheckError_(__FILE__, __LINE__)
GLenum glCheckError_(const char *file, int line);

// We call this right after our OpenGL window is created.
void RNDR_Init(int Width, int Height);
void RNDR_Reset(void);

/* The function called when our window is resized (which shouldn't happen, because we're fullscreen) */
void RNDR_Resize(int Width, int Height);

GLuint RNDR_CreateTextureFromImage(tx_image *img);

static inline void GL_Bind(tx_image *img) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, img->id);
}

/* Related to new aspect ratio */
void RNDR_SetWidescreen(bool wide);
void RNDR_FlipAspect(void);

// settings
extern unsigned int SCR_WIDTH;
extern unsigned int SCR_HEIGHT;
