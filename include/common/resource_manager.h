/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\resource_manager.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Tuesday, January 21st 2020, 12:56:05 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include "common.h"

#if 0
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#endif

typedef void (*destroy_func)(void *);

typedef struct resource_type
{
    destroy_func destroy;
    char type;
} resource_type;

typedef struct resource_object
{
    uint32_t crc;
    void *pointer;
    char type;
    int count;
} resource_object;

void resource_test(void);

/* Object management */
void resource_object_add(char identifier, uint32_t crc, void *pointer);
void resource_object_remove(uint32_t crc);
int resource_object_find(uint32_t crc); 
int resource_object_count(uint32_t crc); 
void *resource_object_pointer(uint32_t crc);

/* Clear all with count <= 0 */
void resource_objects_flush(void);

/* Dump Stats about currently known data */
void resource_print_handlers(void);
void resource_print_objects(void);

void crc32(uint32_t *crc, const uint8_t *data, size_t len);

#endif