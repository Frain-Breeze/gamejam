#ifndef COMMON_H
#define COMMON_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\common.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, June 29th 2019, 6:28:41 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#ifdef _arch_dreamcast
#include <kos.h>
#include <dc/fmath.h>
#include "../dreamcast/cygprofile.h"
#endif

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "resource_manager.h"
#include "file_access.h"
#include "types.h"
#include "stack.h"
#include "../scene/scene.h"
#include "log/log.h"

#ifdef __linux__
#define stricmp strcasecmp
#define strnicmp strncasecmp
#endif

/* atof() breaks under dc if the string is 10 or more characters */
extern float dc_safe_atof(const char* str);
#define atof(x) _Pragma("GCC error \"atof is broken under dreamcast, use dc_safe_atof()\"")

#define FULLSCREEN 1
#define WINDOWED 0
#define WINDOW_TITLE(text, fullscreen) const char *window_title = text; int _FULLSCREEN = fullscreen;

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define ABS(a) (((a) < 0) ? -(a) : (a))
#define CLAMP(x, low, high) (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

#define Q_CIRCLE (M_PI / 2)
#define SX_CIRCLE (M_PI / 4)
#define DEG2RAD(x) (x * M_PI / 180)
#define RAD2DEG(x) (x * 180 / M_PI)

/* Test for GCC > 5.0.0 */
#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)

#if GCC_VERSION > 50000
#define FALLTHROUGH __attribute__((fallthrough));
#else 
#define FALLTHROUGH ;
#endif

//Per system functions
extern float Sys_FloatTime(void);
extern unsigned int Sys_Frames(void);
extern void Sys_Quit(void);
extern bool Sys_IsFullscreen(void);
extern void Sys_SetFullscreen(bool fullscreen);

extern void Game_InputHandler(char c);
extern void Host_Input(float time);

/* Sound System */
int SYS_SND_Destroy(void);
int SYS_SND_Setup(void);

#ifdef PSP
#define printf log_trace
#endif

#endif /* COMMON_H */
