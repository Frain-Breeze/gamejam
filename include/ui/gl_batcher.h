/*
 * File: gl_batcher.h
 * Project: renderer
 * File Created: Saturday, 23rd March 2019 5:42:06 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */

#ifndef __GL_BATCHER__
#define __GL_BATCHER__

#include "common.h"
#include "renderer.h"

#define MAX_BATCHED_SURFVERTEXES 512

extern glvert_fast_t r_batchedfastvertexes_text[1024];
extern uint32_t text_color_packed;
extern int text_size;
extern int r_numsurfvertexes_text;

void R_BeginBatchingSurfacesQuad(void);
void R_EndBatchingSurfacesQuads(void);
//void R_BatchSurfaceQuadText(int x, int y, float frow, float fcol, float size);
static inline void R_BatchSurfaceQuadText(int x, int y, float frow, float fcol, float size)
{
        if ((r_numsurfvertexes_text + 6) >= (MAX_BATCHED_SURFVERTEXES * 2))
                R_EndBatchingSurfacesQuads();

        //Vertex 1
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 0] = (glvert_fast_t){.flags = VERTEX, .vert = {x, y, 0}, .texture = {fcol, frow}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        //Vertex 2
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 1] = (glvert_fast_t){.flags = VERTEX, .vert = {x + text_size, y, 0}, .texture = {fcol + size, frow}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        //Vertex 4
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 2] = (glvert_fast_t){.flags = VERTEX_EOL, .vert = {x, y + text_size, 0}, .texture = {fcol, frow + size}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        //Vertex 4
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 3] = (glvert_fast_t){.flags = VERTEX, .vert = {x, y + text_size, 0}, .texture = {fcol, frow + size}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        //Vertex 2
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 4] = (glvert_fast_t){.flags = VERTEX, .vert = {x + text_size, y, 0}, .texture = {fcol + size, frow}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        //Vertex 3
        r_batchedfastvertexes_text[r_numsurfvertexes_text + 5] = (glvert_fast_t){.flags = VERTEX_EOL, .vert = {x + text_size, y + text_size, 0}, .texture = {fcol + size, frow + size}, .color = { .packed = text_color_packed}, .pad0 = {0}};
        r_numsurfvertexes_text += 6;
}

#endif /* __GL_BATCHER__ */